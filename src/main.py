from typing import Any, Iterable, TextIO
import argparse
import json
import re
import sys

JsonDoc = dict[str, Any]

MATCHER = re.compile(r"^(?P<key>.*?)[:=](?P<value>.*)")


def main():
    """Main entrypoint for command line use cases."""

    cli = argparse.ArgumentParser(description="Convert Java properties files into JSON")
    cli.add_argument("path", nargs="?", help="path to file to parse (stdin by default)")
    opts = cli.parse_args()
    if opts.path:
        doc = file_to_jsondoc(opts.path)
    else:
        doc = text_to_jsondoc(sys.stdin)

    print(json.dumps(doc))


def file_to_jsondoc(path: str) -> JsonDoc:
    """Open the specified file for reading and parse its contents."""

    with open(path) as fh:
        return text_to_jsondoc(fh)


def text_to_jsondoc(source: TextIO) -> JsonDoc:
    """Parse each line in a text source, producing a nested dictionary structure."""

    doc: JsonDoc = {}
    doc_obj: JsonDoc = doc
    key: str = ""

    for line in get_lines(source):
        m = MATCHER.match(line)
        if not m:
            if doc_obj and key:
                # append the current line to the previous value
                old_value = doc_obj[key].rstrip('\\\n')
                value = line.strip(" \\\n")
                doc_obj[key] = f"{old_value}{value}"

            continue

        key = m.group("key")
        value = m.group("value")

        doc_obj = doc
        if "." in key:
            # create a series of nested dictionaries for each dot-separated string in the key
            *path, key = key.split(".")
            for _key in path:
                if _key not in doc_obj:
                    doc_obj[_key] = {}
                doc_obj = doc_obj[_key]

        try:
            # handle some basic value parsing (integers, floats, booleans, etc)
            value = json.loads(value)
        except Exception:
            pass

        doc_obj[key] = value

    return doc


def get_lines(source: TextIO) -> Iterable[str]:
    """Iterate over the lines in a text source, ignoring empty lines and comments."""

    for line in source:
        stripped_line = line.strip()

        # skip empty lines
        if not stripped_line:
            continue

        # skip comments
        if stripped_line.startswith("!") or stripped_line.startswith("#"):
            continue

        yield line


if __name__ == "__main__":
    main()
